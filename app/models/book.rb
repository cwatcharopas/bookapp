class Book < ActiveRecord::Base
  validates :title, presence: { message: I18n.t("messages.validate.title") }
  validates :author, presence: { message: I18n.t("messages.validate.author") }
  belongs_to :author
end

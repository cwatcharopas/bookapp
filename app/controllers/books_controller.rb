class BooksController < ApplicationController
  before_action :get_page
  before_action :get_book, only: [:edit, :update, :destroy]
  before_action :get_books, only: [:index, :edit]

  def index
    @book = Book.new(released_date: Time.now)
  end

  def create
    @book = Book.new(book_params)

    if @book.save
      redirect_to books_path, notice: 'Book was successfully added.'
    else
      render action: 'index'
    end
  end

  def edit
    render action: 'index'
  end

  def update
    if @book.update(book_params)
       redirect_to books_path, notice: 'Book was successfully updated.'
    else
       render action: 'index'
    end
  end

  def destroy
    @book.destroy
    redirect_to books_path, notice: "#{@book.title} was successfully deleted."
  end

  def about
  end

private
    # Use callbacks to share common setup or constraints between actions.
    def get_page
      @@page ||= 1
      @@page = params[:page].to_i if params[:page]
    end
    def get_book
      @book = Book.find(params[:id])
    end
    def get_books
      @books = Book.paginate(page: @@page, per_page: 10)
      if @@page > 1 && @books.empty?
        @@page -= 1
        @books = Book.paginate(page: @@page, per_page: 10)
      end
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def book_params
      params.require(:book).permit(:title, :released_date, :author_id,
        :rating, :available)
    end
end
